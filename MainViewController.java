package by.stormnet.fxfigures.client.controller;

import by.stormnet.fxfigures.client.controller.MainViewController.1;
import by.stormnet.fxfigures.client.controller.geometry.Circle;
import by.stormnet.fxfigures.client.controller.geometry.Figure;
import by.stormnet.fxfigures.client.controller.geometry.Rectangle;
import by.stormnet.fxfigures.client.controller.geometry.Triangle;
import java.net.URL;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class MainViewController implements Initializable {
    private List<Figure> figures = new LinkedList();
    private Random rnd = new Random(System.currentTimeMillis());
    @FXML
    private Canvas canvas;

    public MainViewController() {
    }

    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void onMouseClicked(MouseEvent event) {
        System.out.println("x: " + event.getX());
        System.out.println("y: " + event.getY());
        this.addFigure(this.createFigure(event.getX(), event.getY()));
        this.repaint();
    }

    private void addFigure(Figure figure) {
        this.figures.add(figure);
    }

    private Figure createFigure(double x, double y) {
        Figure figure = null;
        double lineWidth = (double)this.rnd.nextInt(12);
        switch(this.rnd.nextInt(3)) {
            case 0:
                System.out.println("create circle");
                double radius = (double)this.rnd.nextInt(101);
                figure = new Circle(x, y, lineWidth == 0.0D ? 1.0D : lineWidth, Color.RED, radius < 10.0D ? 10.0D : radius);
                break;
            case 1:
                System.out.println("create rectangle");
                double width = (double)this.rnd.nextInt(101);
                double height = (double)this.rnd.nextInt(101);
                figure = new Rectangle(x, y, lineWidth == 0.0D ? 1.0D : lineWidth, Color.CORNFLOWERBLUE, width < 10.0D ? 10.0D : width, height < 10.0D ? 10.0D : height);
                break;
            case 2:
                System.out.println("create triangle");
                double base = (double)this.rnd.nextInt(101);
                figure = new Triangle(x, y, lineWidth == 0.0D ? 1.0D : lineWidth, Color.GREEN, base < 10.0D ? 10.0D : base);
                break;
            default:
                System.out.println("unknown figure type.");
        }

        return (Figure)figure;
    }

    private void repaint() {
        if (this.figures != null) {
            this.canvas.getGraphicsContext2D().clearRect(0.0D, 0.0D, 1280.0D, 800.0D);
            Iterator var1 = this.figures.iterator();

            while(var1.hasNext()) {
                Figure figure = (Figure)var1.next();
                if (figure != null) {
                    figure.draw(this.canvas.getGraphicsContext2D());
                }
            }

        }
    }

    public void Result(MouseEvent mouseEvent) {
        this.printAllFiguresInfo(this.figures);
        this.printOverSquarble(this.figures);
    }

    private void printAllFiguresInfo(List<Figure> figures) {
        Iterator var2 = figures.iterator();

        while(var2.hasNext()) {
            Figure figure = (Figure)var2.next();
            figure.printInfo();
        }

        System.out.println("________________________________________");
    }

    private void printOverSquarble(List<Figure> figures) {
        double squar = 0.0D;

        Figure figure;
        for(Iterator var4 = figures.iterator(); var4.hasNext(); squar += figure.getSquarblePrint()) {
            figure = (Figure)var4.next();
        }

        System.out.printf("Total area = %.5f\n", squar);
        System.out.println("________________________________________");
    }

    public void Result2(MouseEvent mouseEvent) {
        Set<Figure> figures1 = new TreeSet((new 1(this)).thenComparing((o1, o2) -> {
            return (int)(o1.getSquarblePrint() - o2.getSquarblePrint());
        }));
        Iterator var3 = this.figures.iterator();

        Figure figs;
        while(var3.hasNext()) {
            figs = (Figure)var3.next();
            figures1.add(figs);
        }

        var3 = figures1.iterator();

        while(var3.hasNext()) {
            figs = (Figure)var3.next();
            System.out.println(figs);
        }

    }
}

